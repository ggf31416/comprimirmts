#http://stackoverflow.com/questions/15652594/how-to-find-files-with-specific-case-insensitive-extension-names-in-python?lq=1
import sys
import glob,os,subprocess,fcntl, shutil, time
import logging

# grupo bajo el cual se deben crean los directorios y los archivos
_grupo = "openfing"

def main(argv):
    
    extensiones=['.mp4','.mts'] # mp4 solo para pruebas
    print(argv)

    #extensiones=['.mts']
    videos = []

    # flags
    debo_respaldar_mp4 = False      # si no hay riesgo de corrupcion con mega, ya no precisamos hacer otra copia del mp4
    copiar_rutas_relativas = True   # Si el input es [Inbox]/a/b/x.MTS cada salida va a [Salida]/a/b/

    # *** las rutas DEBEN terminar en /        ***
    ruta_procesando = "../Procesando/"
    ruta_syncro =  "../../Salida_MP4/" # "../Convertido/"


    ruta_respaldo_mp4 = "../Respaldo_MP4/" if debo_respaldar_mp4 else ruta_syncro
    ruta_respaldo = "../../[Originales]/"
    ruta_inbox = "../../Inbox/"

    crear_dirs([ruta_inbox,ruta_procesando,ruta_syncro,ruta_respaldo_mp4,ruta_respaldo])

    logging.basicConfig(filename=ruta_inbox + "log.log", format='[%(levelname)s] %(asctime)s - %(message)s',level=logging.DEBUG)

    if len(argv) == 1:
        print("Solo se creo directorios");
        print("Uso: python3 procesarArchivo.py archivo.MTS");
        sys.exit()

    f = argv[1]
    if (os.path.isfile(f) == False):
        logging.debug("No existe " + f);
        sys.exit()

    # primero busco archivos y los muevo al directorio 'Procesando'
    # si los dejara en pendientes podria no haber terminado la conversion antes de la proxima corrida del cron
    nombre = os.path.split(f)[1]
    sinExt, ext = os.path.splitext(nombre)
    relPath = "";
    #print sinExt, ext, ext.lower()
    if ext.lower() in extensiones:
        # por seguridad intentaba ver si esta en uso, pero no surgia efecto con el copiar
        try:
            #fd = open(f,"a+");
            # obtengo el path relativo para espejarlo en la salida
            relPath = os.path.split(os.path.relpath(f,ruta_inbox))[0] if copiar_rutas_relativas else ""
        except IOError:
            logging.debug(f + " esta bloqueado en el momento")
        else:
            #fd.close();
            set_permisos(f);
            ruta_respaldo_r = os.path.join(ruta_respaldo,relPath)
            nombreProc = mover_seguro(f,ruta_respaldo_r)
            videos.append(nombreProc)
            logging.info("mv " + f + " --> " + ruta_respaldo)

    # puedo usar un file lock para que no se ejecute varias veces
    lock = pedir_bloqueo()

    for v in videos:
        nombre = os.path.split(v)[1]
        noExt = os.path.splitext(nombre)[0]
        resultado = encontrar_nombre_libre(noExt+"_edit.mp4",ruta_procesando )

        nombre_stat = encontrar_nombre_libre("ffmpeg_stats",ruta_procesando)
        open(nombre_stat, 'a+').close() # HACK: crea el archivo para no depender del sufijo de ffmpeg al detectar duplicados
        logging.info("Inicia conversion " + v + " --> " + resultado)
        log_script = open(os.path.join(ruta_procesando,"log_conversion.log"), 'a+')
        # corre la conversion con baja prioridad
        retval = subprocess.call(["/usr/bin/nice", "-n 9", "./convertir_auto_V2.sh",v,resultado,nombre_stat],shell=False,stdout=log_script,stderr=subprocess.STDOUT)
        #retval = subprocess.call(["/usr/bin/nice", "-n 12", "./convertir_normal.sh",v,resultado,nombre_stat],shell=False,stdout=log_script,stderr=subprocess.STDOUT)
        if retval == 0:
            if debo_respaldar_mp4:
                # respalda el mp4 creado en otra carpeta antes de moverlo a sync
                ruta_respaldo_mp4_r = os.path.join(ruta_respaldo_mp4, relPath)
                nombre_dest = mover_seguro(resultado,ruta_respaldo_mp4_r)
                set_permisos(nombre_dest)
                logging.info("mv " + resultado + " --> " + nombre_dest)
                # mueve edicion a convertido
                ruta_syncro_r = os.path.join(ruta_syncro , relPath)
                nombre_dest2 = copiar_seguro(nombre_dest,ruta_syncro_r)
                set_permisos(nombre_dest2)
                logging.info("cp " + nombre_dest + " --> " + nombre_dest2)
            else:
                ruta_syncro_r = os.path.join(ruta_syncro,relPath)
                nombre_dest = mover_seguro(resultado,ruta_syncro_r)
                set_permisos(nombre_dest)
                logging.info("mv " + resultado + " --> " + nombre_dest)
        else:
            logging.error("ERROR en conversion " + v + " --> " + resultado + ", ver log en " + ruta_procesando + " para mas info")
    liberar_bloqueo(lock)

def crear_dirs(directorios):
    print(directorios)
    for d in directorios:
        try:
            if not os.path.isdir(d):
                os.makedirs(d,exist_ok=True)
                shutil.chown(d,group=_grupo)
                subprocess.call(["chmod", "g+s",d]);
        except Exception as e:
            logging.error("[crear_dirs " + d + " ] " + repr(e))

def set_permisos(f):
    try:
        shutil.chown(f,group=_grupo)
        subprocess.call(["chmod", "g+rw,o+r",f]);
    except Exception as e:
        logging.error("[set_permisos " + f + " ] " + repr(e))

def encontrar_nombre_libre(f,dir_dest):
    if not dir_dest.endswith("/"):
        dir_dest = dir_dest + "/"
    if  not os.path.isdir(dir_dest):
        crear_dirs([dir_dest])
    nombre = os.path.split(f)[1]
    noExt,ext = os.path.splitext(nombre)
    intentos = 0
    f_destino = dir_dest + nombre
    while os.path.isfile(f_destino):
        intentos += 1;
        f_destino = dir_dest + noExt + "_" + str(intentos).zfill(3) + ext
    return f_destino

#mueve el archivo al directorio especificado, cambiando el nombre si ya existe
def mover_seguro(f, dir_dest):
    f_dest = encontrar_nombre_libre(f,dir_dest)
    #print f_dest
    #print f
    try:
        os.rename(f,f_dest)
    except:
        logging.error("Fallo en mv " + f + " --> " + f_dest)
        raise
    return f_dest

def copiar_seguro(f,dir_dest):
    f_dest = encontrar_nombre_libre(f,dir_dest)
    try:
        shutil.copy2(f,f_dest)
    except:
        logging.error("Fallo en cp " + f + " --> " + f_dest)
        raise
    return f_dest




# las siguientes 2 funciones obtienen un bloqueo para que no corra 2 veces el script
# en teoria se libera si se sale del script aunque se caiga
def pedir_bloqueo():
    file_desc = open("./__lock",'a+')
    while True:
        try:
            # intento obtener bloqueo exclusivo por metodo de file block
            fcntl.flock(file_desc,fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            time.sleep(15) # vuelvo a intentar en 15 segundos
        else:
            return file_desc

def liberar_bloqueo(file_desc):
    fcntl.flock(file_desc,fcntl.LOCK_UN)



if __name__ == "__main__":
    main(sys.argv)
