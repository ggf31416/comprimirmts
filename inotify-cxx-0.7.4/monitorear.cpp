/*
Basado en:
Simple inotify-cxx example which will output all events for /tmp.
(Author: Thomas Jarosch <thomas.jarosch@intra2net.com>
Licensed under the same licenses as inotify-cxx.)

+ Muchos ejemplos de stackoverflow

Copyright 2015 OpenFing  (todos los bugs son propiedad de Guillermo Gabrielli ;-) )

*/

//importo la mitad de c++...
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <dirent.h>
#include <stdio.h>
#include <string>
#include <locale>
#include <exception>
#include <iostream>
#include <map>
#include <set>
#include <queue>
#include <thread>         // std::thread
#include <mutex>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "inotify-cxx.h"



using namespace std;

struct Elemento{
    string ruta;
    time_t tiempo;

    Elemento(string r, time_t t){
        ruta = r;
        tiempo = t;
    }
};



// variables
std::mutex mtx;         // mutex for critical section
std::mutex empty;       // mutex para esperar que haya algo en la cola
std::mutex puedoProcesar;
std::queue<Elemento*> cola;
std::map <string,InotifyWatch*> watches;
std::set <InotifyWatch*> valid;
string actual = ""; // quiero ignorar eventos del archivo que esta siendo procesado actualmente
uint32_t mascara = IN_CLOSE_WRITE |  IN_MOVED_FROM | IN_MOVED_TO | IN_CREATE  | IN_DELETE_SELF;
int filedesc = 0;

// No son constantes, sino que se inicializan por línea de comandos
int ESPERA;
string WATCH_DIR;

//http://stackoverflow.com/questions/874134/find-if-string-endswith-another-string-in-c
inline bool ends_with(string const & value, string const & ending)
{
    if (ending.size() > value.size()) return false;
    return equal(ending.rbegin(), ending.rend(), value.rbegin());
}



bool esMTS(const string  &ruta){
    try{
        string upper = ruta;
        std::transform(upper.begin(), upper.end(),upper.begin(), ::toupper);
        return ends_with(upper,".MTS") || ends_with(upper,".MP4");
    }
    catch(exception &e){
        cerr << "STL exception occured en esMTS: " << e.what() << endl;
    }

}

bool existe(const string  &ruta){
    //http://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c-cross-platform
    return access( ruta.c_str(), F_OK ) != -1;
}

bool esDir(string filename){
    struct stat st;
    int code = stat(filename.c_str(),&st);
    return (code == 0) && ((st.st_mode & S_IFDIR) != 0);
}


// llama al script para procesar y bloquea hasta que termine
void comprimir(const string  &ruta){
    if (existe(ruta)){
        std::cout << "Comprimir " << ruta <<  std::endl;
        actual = ruta;
        //con suerte 1000 chars es suficiente para la ruta
        char comando[1000];
        snprintf(comando,1000,"python3 ./procesarArchivo.py \"%s\" &",ruta.c_str());
        system(comando);
        actual = "";
    }
    else{
        std::cout << "No exite " << ruta <<  std::endl;
    }
}

// libero todos los locks (para caso de error donde no se en que estado estoy)
void liberar_locks(){
    mtx.unlock();
    empty.unlock();
    puedoProcesar.unlock();
}


void procesar(){
    puedoProcesar.lock();
    std::cout << "Inicio Procesar" << std::endl;
    try{
        unsigned int dormir = 1000000; // 1seg
        bool proceso = false;
        for (;;){
            mtx.lock(); // *** bloqueo mutex, debo liberarlo en todos los caminos antes de hacer algo que bloquee el thread ***
            if (!cola.empty()){
                Elemento* e = cola.front();
                time_t now = time(NULL);
                double seg_diff = difftime(now,e->tiempo);
                // si paso la espera lo saco y proceso, por otro lado si fue agregado en el futuro tambien lo proceso porque se cambio la hora
                if (seg_diff > ESPERA || seg_diff < 0){
                    cola.pop();
                    mtx.unlock(); // *** libero mutex ***
                    comprimir(e->ruta);
                    delete e;
                }
                else{
                    // duermo hasta que haya pasado la espera (+ 10 ms por errores de redondeo)
                    dormir = (unsigned int)((10 + seg_diff * 1000 ) * 1000);
                    mtx.unlock(); // *** libero mutex ***
                    usleep(dormir);
                }
            }
            else{
                mtx.unlock(); // *** libero mutex ***
                empty.lock(); // duermo hasta que agregen algo a la cola, al mejor estilo Fried
            }
        }
    }
    catch(exception &e){
        cerr << "STL exception occured en procesar: " << e.what() << endl;
        liberar_locks();
    }
    puedoProcesar.unlock();
    std::cout << "Fin Procesar" << std::endl;
}


void listarWatches(){
    std::cout << "Mapa watches:" << std::endl;
    std::map<string,InotifyWatch*>::iterator it = watches.begin();
    for(it;it != watches.end();++it){
        std::cout << it->first << std::endl;
    }
}

void listarWatches(std::map<string,InotifyWatch*>::iterator it){
    std::cout << "Mapa watches >=:" << std::endl;
    for(it;it != watches.end();++it){
        std::cout << it->first << std::endl;
    }
}

// agrego el watch si ya no existe
bool agregarSubWatch(Inotify& notify, const string& r){
    string ruta = ('/' != r[r.length() - 1]) ? r + "/" : r;
    std::map<string,InotifyWatch*>::iterator it;
    it = watches.find(ruta);
    if (it == watches.end()){
        //cout << "agregar watch: " << ruta << endl;
        InotifyWatch* watch = new InotifyWatch(ruta, mascara);
        notify.Add(watch);
        valid.insert(watch);
        watches[ruta] = watch;
        return true;
    }
    //cout << "ya existe watch: " << ruta << endl;
    return false;
}

// elimino el watch sobre un directorio y todos los subdirectorios
void eliminar_recursivo(Inotify& notify, string r){
    try{
        //cout << "eliminar watch: " << r << endl;
        string ruta = ('/' != r[r.length() - 1]) ? r + "/" : r;
        std::map<string,InotifyWatch*>::iterator it;
        //listarWatches(watches.find(ruta));
        it = watches.find(ruta);
        if (it != watches.end()){
            // map de c++ es una estructura de arbol, esta ordenado lexicograficamente
            while (it != watches.end() && // si no llegue al final
            // como las claves terminan en si ruta es prefijo de la clave entonces es un subdirectorio o es el directorio ruta mismo
            it->first.compare(0,ruta.length(),ruta) == 0){
                std::map<string,InotifyWatch*>::iterator borrar = it;
                ++it;

                InotifyWatch* watch = borrar->second;
                //cout << "elimino: " << borrar->first << endl;
                notify.Remove(watch);
                valid.erase(watch);
                watches.erase(borrar);
                delete watch;
                if (it != watches.end()) std::cout << it->first << " Prox?: " << ruta.compare(0,ruta.length(),it->first) << " len " << ruta.length() << std::endl;
            }
        }
    }
    catch(exception& e){
        cerr << "Error al eliminar: " << e.what() << endl;
    }
    catch(...){
        cerr << "Error desconocido al eliminar: "  << endl;
    }

}

/*void eliminarSubWatch(Inotify& notify, string ruta){
cout << "eliminar watch: " << ruta << endl;
std::map<string,InotifyWatch*>::iterator it;
it = watches.find(ruta);
if (it != watches.end()){
InotifyWatch* watch = it->second;
notify.Remove(watch);
watches.erase(it);
delete watch;
}
else if ('/' == ruta[ruta.length() - 1] ){ // si termina en '/'
// me fijo si no esta sin la /
string ruta2 = ruta.substr(0,ruta.length() - 1);
eliminarSubWatch(notify, ruta2);
}
}*/




void agregarArchivo(string ruta,time_t now){
    if (esMTS(ruta)){
        Elemento* e = new Elemento(ruta,now); // se agrego un archivo
        cola.push(e);
        empty.unlock();
    }
}

string unir(const string& a,const string& b){
    string a2 = ('/' != a[a.length() - 1]) ? a + "/" : a;
    return a2 + b;
}


void chequearSalir(const char* nombre){
	if (strcmp("__salir_monitorear__",nombre) == 0){
		printf("Error: Se cumplio Condicion de Salida:\n");
		printf("Se encontro un archivo con nombre \"__salir_monitorear__\"\n");
		liberar_locks();
		exit(2);
	}
}

//http://stackoverflow.com/questions/8436841/how-to-recursively-list-directories-in-c-on-linux
void agregar_recursivo(string n, int level,Inotify& notify)
{
    try{
        const char *name = n.c_str();
        time_t now = time(NULL);
        DIR *dir;
        struct dirent *entry;
        if (!(dir = opendir(name))) return;
        if (!(entry = readdir(dir))) return;
        do {
            string ruta = name;
            ruta = unir(ruta,entry->d_name);
            if (entry->d_type == DT_DIR) {
                char path[1024];
                int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
                path[len] = 0;
                if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
                //printf("%*s[%s]\n", level*2, "",ruta.c_str());
                agregarSubWatch(notify,ruta);
                agregar_recursivo(path, level + 1,notify);
            }
            else{
                //printf("%*s- %s\n", level*2, "", ruta.c_str());
				chequearSalir(entry->d_name);
                agregarArchivo(ruta,now);
            }
        } while (entry = readdir(dir));
        closedir(dir);
    }
    catch(exception& e){
        cerr << "Error al agregar: " << e.what() << endl;
    }
    catch(...){
        cerr << "Error desconocido al agregar: "  << endl;
    }

}



void monitorear(){
    bool ok = false;
    try {
        Inotify notify;
        InotifyWatch watch(WATCH_DIR, mascara);
        notify.Add(watch);
        valid.insert(&watch);
        agregar_recursivo(WATCH_DIR,4,notify);
        listarWatches();
        puedoProcesar.unlock(); // permito el procesamiento

        for (;;) {
            try{
                ok = false;
                notify.WaitForEvents();

                mtx.lock(); // pido acceso exclusivo a cola
                time_t now = time(NULL);

                size_t count = notify.GetEventCount();
                while (count > 0) {
                    count--;
                    InotifyEvent event;
                    bool got_event = notify.GetEvent(&event);
                    //cout << "Pending events: " << count << endl;
                    if (got_event) {
                        string mask_str;
                        event.DumpTypes(mask_str);
                        cout << "Op: \"" << mask_str << "\", ";
                        //cout << "event cookie: \"" << event.GetCookie() << "\", ";
                        string filename = event.GetName();
                        InotifyWatch* w = event.GetWatch();
                        if (valid.count(w) == 0) {
                            std::cout << "Watch invalid!!!" << std::endl;
                            continue;
                        }
                        string wpath = w->GetPath();
                        string ruta = unir(wpath,filename);
                        //cout << "[watch " << watch_dir << "] ";

                        cout << "ruta: \"" << ruta << "\"" << endl;

                        uint32_t ev_mask = event.GetMask();

                        if (ev_mask & IN_ISDIR){
                            // Es un Directorio, manejo los watches
                            if (ev_mask & IN_CREATE){
                                agregarSubWatch(notify,ruta);
                                //listarWatches();
                            }
                            else if (ev_mask & IN_MOVED_TO){
                                // se movio un directorio con destino adentro del inbox
                                // si ya no existe el watch agrego los subdirectorios y los archivos
                                if (agregarSubWatch(notify,ruta)) agregar_recursivo(ruta,4,notify);
                                //listarWatches();
                            }
                            else if (ev_mask & (IN_MOVED_FROM | IN_DELETE_SELF) ){
                                eliminar_recursivo(notify,ruta);
                                //listarWatches();
                            }
                        }

                        else{
                            // Es un Archivo, lo agrego
                            if (ev_mask & (IN_CLOSE_WRITE | IN_MOVED_TO) ){
                                // ignoro eventos del archivo actual, ya que lo abro para escritura para verificar que no este siendo usado
                                chequearSalir(filename.c_str());
                                if (actual.compare(ruta) != 0){
                                    agregarArchivo(ruta,now);
                                }
                            }
                            else if (ev_mask & (IN_DELETE | IN_MOVED_FROM)){
                                //puedo simplemente ignorar los archivos en la llamada a procesar, no necesito eliminarlos de la cola
                            }
                        }
                    }
                }
                mtx.unlock(); // libero acceso exclusivo a cola
                ok = true;
            } catch (InotifyException &e) {
                cerr << "Inotify exception occured: " << e.GetMessage() << endl;
            } catch (exception &e) {
                cerr << "STL exception occured: " << e.what() << endl;
            } catch (...) {
                cerr << "unknown exception occured" << endl;
            }
            if (!ok){
                liberar_locks();
            }
        }
    } catch (InotifyException &e) {
        cerr << "Inotify exception occured: " << e.GetMessage() << endl;
    }
    liberar_locks();
}


// las siguientes 2 funciones obtienen un bloqueo para que no corra 2 veces el script
// en teoria se libera si se sale del script aunque se caiga
bool pedir_lockf(){
    filedesc = open("./__mon_lock", O_WRONLY | O_CREAT);
    system("chmod 666 ./__mon_lock");
    lseek(filedesc, 0L, 0); // make sure the file pointer is at the beginning of the file.
    return (lockf(filedesc, F_TLOCK, 0L) == 0);
}

void liberar_lockf(){
    lockf(filedesc,F_ULOCK, 0);
}

int main(int argc, char** argv){

    bool error = true;
    if (argc >= 2){
        ESPERA = atoi(argv[1]);
        if (ESPERA >= 0){
            if (argc == 2){
                WATCH_DIR = "../Inbox";
            }
            else{
                WATCH_DIR = argv[2];
            }
            error = false;
        }
    }
    if (error){
        printf("Uso:\n");
        printf("monitorear Espera [\"ruta al Inbox\"]\n");
        printf("Espera es la cantidad de segundos >= 0 minima que espera antes de iniciar la compresion.\n");
        printf("Si no se especifica la ruta del inbox se usara \"../Inbox\".\n");
        printf("Los directorios dentro del Inbox son monitoreados recursivamente\n");
    }
    else{
	if (!pedir_lockf()){
        	printf("Error: Otra instancia se esta ejecutando desde el mismo directorio\n");
        	exit(1);
    	}
        printf("Monitoreando %s\n", WATCH_DIR.c_str());
        //http://stackoverflow.com/questions/266168/simple-example-of-threading-in-c
        puedoProcesar.lock();
        thread t1(monitorear); // ejecuto bucle de monitoreo
        thread t2(procesar); // ejecuto bucle de procesamiento
        t1.join();
	liberar_lockf();
    }
    
}
