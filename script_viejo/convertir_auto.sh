#!/bin/sh

# $1 origen mts, $2 destino mp4, $3 nombre para el archivo de estadisticas
#conversion en 2 pasos

# ruta al ffmpeg utilizado
#FFMPEG=ffmpeg
FFMPEG="../bin/ffmpeg-git"
#FFMPEG=avconv
# 

DN="hqdn3d=1.5:1.5:6.0:5.0" # filtro denoising, principalmente temporal, no deberia afectar mucho la calidad aunque fuera aplicado en un video sin ruido



FILTRO1="fps=fps=25,setdar=dar=16/9"
FILTRO2="fps=fps=25,setdar=dar=16/9"

mediainfo "$1"  | grep -i "Scan.*type.*Progressive"
if [ "$?" -eq "0" ]; then # grep devuelve 0 si no encuentra el patron, 1 si no
	 FILTRO1="$FILTRO1"
	 FILTRO2="$FILTRO2"
else 
    mediainfo "$1"  | grep -i "Scan.*type.*Interlaced"
    if [ "$?" -eq "0" ]; then     # es entrelazado, convierto a progresivo
	    FILTRO1="yadif=0:0:0,${FILTRO1}" # filtro más rápido para usar en el primer paso
	    FILTRO2="yadif=0:0:0,${FILTRO2}" # filtro más rápido para usar en el primer paso
	    #FILTRO2="w3fdif,framestep=2," # filtro más lento para usar en el segundo paso
    else
        # No es progresivo ni entrelazado, es alguna mezcla o mediainfo dio error, lo corro con yadif 
        # Espero que no pase...
        FILTRO1="yadif=0:-1:0,${FILTRO1}"
	    FILTRO2="yadif=0:-1:0,${FILTRO2}"
        echo mediainfo "$1"  | grep -i "Scan.*type.*"
	    echo "##### Scan type Desconocido o MediaInfo no esta instalado !!!  #####"
    fi
fi


ORIGINAL="$1"
ORIGINAL="${2}.temporal.mp4"
/usr/bin/time -a -f "[${0##*/} pre] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t"  $FFMPEG -loglevel warning -y -i "$1" -vcodec copy -acodec aac -strict -2 -b:a 256k "$ORIGINAL"


# comprimo 1 minuto para ver el ssim (mide el error al comprimir el video, mas alto menos error) si es muy bajo suele ser por el ruido y es dificil de comprimir por lo que le pongo denoising
ssim=$($FFMPEG  -y -i "$ORIGINAL" -t 60 -g 200 -vcodec libx264 -preset fast -tune ssim -vf yadif=0:0:0,fps=fps=25 -b:v 2000k -an -ssim 1 "${2}-test.mp4" 2>&1 | grep -oP "SSIM Mean.*[0-9][0-9\.]*")
echo $ssim
# tomo solo 2 digitos del ssim
ssim=$(echo $ssim | grep -oP '(\([0-9][0-9]?)' | grep -oP '[0-9][0-9]?')

# si el ssim es menor a 17 considero que es ruidoso y le aplico el denoising
if [ "$ssim" -lt "17" ]; then
	FILTRO1="${FILTRO1},${DN}"
	FILTRO2="${FILTRO2},${DN}"
fi

set -x

# seria mejor usar 4 o 5 bframes pero lleva mas tiempo y las tarjetas nvidia en linux suelen presentar rallas al decodificar si hay muchos bframes
/usr/bin/time -a -f "[${0##*/} 1p] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t"  /usr/bin/time -a -f "%%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t 1p ${0##*/}"  $FFMPEG -loglevel warning -y -i "$ORIGINAL" -g 200 -vcodec libx264 -passlogfile "$3" -pass 1 -preset medium -b_strategy 2 -tune film -vf ${FILTRO1} -b:v 2000k -an -f mp4 /dev/null
/usr/bin/time -a -f "[${0##*/} 2p] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t"  /usr/bin/time -a -f "%%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t 1p ${0##*/}"  $FFMPEG -loglevel warning -y -i "$ORIGINAL" -g 200 -vcodec libx264 -passlogfile "$3" -pass 2 -preset medium -b_strategy 2 -subq 7 -me_method umh -tune film -vf ${FILTRO2}  -b:v 2000k -acodec copy -movflags +faststart "$2"



ERR=$?
set +x

rm "$ORIGINAL"
rm "${3}-0.log"
rm "${3}-0.log.mbtree"
rm "${2}-test.mp4"
rm "$3"

exit $ERR
