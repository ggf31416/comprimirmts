#!/bin/sh

# un poco mas lento que las opciones por defecto pero no deberia usar mas recursos al decodificar 
# $1 origen mts, $2 destino mp4, $3 nombre para el archivo de estadisticas
#conversion en 2 pasos

# ruta al ffmpeg utilizado
#FFMPEG=ffmpeg
FFMPEG="../bin/ffmpeg"
#FFMPEG=avconv
# 

# aparentemente no funciona el opencl en la maquina de oficina
#$FFMPEG -version  | grep -i "enable-opencl"
#OPENCL=$((1-$?)) # 1 si el build tiene opencl, 0 si no

$FFMPEG -version  | grep -i "enable-libfdk-aac"
if [ "$?" -eq "0" ]; then # 0 si el build tiene libfdk, 0 si no
	AUDIO_CODEC="-c:a libfdk_aac"
else
	AUDIO_CODEC="-c:a aac -strict -2"
fi

BITRATE="2000k"

DN_L1="hqdn3d=1.5:1.5:2.5:1.5" # filtro denoising ligero ("nivel 1 de denoising")
DN_L2="hqdn3d=2.0:2.0:5.0:4.0,unsharp=la=0.1" # :opencl=${OPENCL}" # filtro denoising un poco mas fuerte + unsharp ligero para compensar ("nivel 2 de denoising")




FILTRO1="fps=fps=25,setdar=dar=16/9"
FILTRO2="fps=fps=25,setdar=dar=16/9"

mediainfo "$1"  | grep -i "Scan.*type.*Progressive"
if [ "$?" -eq "0" ]; then # grep devuelve 0 si no encuentra el patron, 1 si no
	 FILTRO1="$FILTRO1"
	 FILTRO2="$FILTRO2"
else 
    mediainfo "$1"  | grep -i "Scan.*type.*Interlaced"
    if [ "$?" -eq "0" ]; then     # es entrelazado, convierto a progresivo
	    FILTRO1="yadif=0:0:0,${FILTRO1}" # filtro más rápido para usar en el primer paso
	    FILTRO2="yadif=0:0:0,${FILTRO2}" # filtro más rápido para usar en el primer paso
	    #FILTRO2="w3fdif,framestep=2," # filtro más lento para usar en el segundo paso
    else
        # No es progresivo ni entrelazado, es alguna mezcla o mediainfo dio error, lo corro con yadif 
        # Espero que no pase...
        FILTRO1="yadif=0:-1:0,${FILTRO1}"
	    FILTRO2="yadif=0:-1:0,${FILTRO2}"
        echo mediainfo "$1"  | grep -i "Scan.*type.*"
	    echo "##### Scan type Desconocido o MediaInfo no esta instalado !!!  #####"
    fi
fi

# comprimo 1 minuto para ver el ssim (mide el error al comprimir el video, mas alto menos error) si es muy bajo suele ser por el ruido y es dificil de comprimir por lo que le pongo denoising
ssim=$($FFMPEG  -y -i "$1" -t 60 -g 200 -vcodec libx264 -preset fast -tune ssim -vf yadif=0:0:0,fps=fps=25 -b:v 2000k -an -ssim 1 "${2}-test.mp4" 2>&1 | grep -oP "SSIM Mean.*[0-9][0-9\.]*")
echo $ssim
# tomo solo 2 digitos del ssim
ssim=$(echo $ssim | grep -oP '(\([0-9][0-9]?)' | grep -oP '[0-9][0-9]?')

# $ORIGINAL es el archivo temporal en el que remuxeamos a mp4 para evitar la falla en el 2do paso con ffmpeg
ORIGINAL="${2}.temporal.mp4"
# usa libfdk si esta disponible, sino aac interno
/usr/bin/time -a -f "[${0##*/} pre] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t"  $FFMPEG -loglevel warning -y -i "$1" -vcodec copy $AUDIO_CODEC -b:a 256k "$ORIGINAL"

# si el ssim es menor a 17 considero que es ruidoso y le aplico el denoising
if [ "$ssim" -lt "17" ]; then
   
    if [ "$ssim" -lt "15" ]; then  # Si el ruido es verdaderamente malo
        BITRATE="2200k" # aumento un poquito mas el bitrate para asegurarme que quede bien
    	FILTRO1="${FILTRO1},${DN_L2}"
	    FILTRO2="${FILTRO2},${DN_L2}"
	else 
	    BITRATE="2100k" # aumento un poquito el bitrate para asegurarme que quede bien
	    FILTRO1="${FILTRO1},${DN_L1}"
	    FILTRO2="${FILTRO2},${DN_L1}"
    fi
fi

set -x


/usr/bin/time -a -f "[${0##*/} 1p] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\t" $FFMPEG -loglevel warning -y -i "$ORIGINAL" -g 200 -vcodec libx264 -passlogfile "$3" -pass 1 -preset slow -trellis 2 -refs 4  -tune film -direct-pred spatial -vf ${FILTRO1} -b:v $BITRATE -an -f mp4 /dev/null
/usr/bin/time -a -f "[${0##*/} 2p] %%CPU:,%P, tiempo real:,%E,tiempo usuario:,%U,\nOrigen: ${1}" $FFMPEG -loglevel warning -y -i "$ORIGINAL" -g 200 -vcodec libx264 -passlogfile "$3" -pass 2 -preset slow -trellis 2 -refs 4  -tune film -direct-pred spatial -vf ${FILTRO2}  -b:v $BITRATE -acodec copy -ssim 1 -movflags +faststart "$2"



ERR=$?
set +x

rm "$ORIGINAL"
rm "${3}-0.log"
rm "${3}-0.log.mbtree"
rm "${2}-test.mp4"
rm "$3"

exit $ERR
